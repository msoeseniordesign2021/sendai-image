SENDAI_DEPENDENCIES += rpi-firmware
SENDAI_INSTALL_IMAGES = YES
ERPAIM_LOGO_LINES = `wc -l ${BR2_PACKAGE_SENDAI_BOOT_SPLASH} | awk '{print $$1}'`

ifeq ($(BR2_PACKAGE_SENDAI_X11_NOCURSOR),y)
define SENDAI_INSTALL_X11_ARGS
	echo -n "-nocursor" > $(TARGET_DIR)/etc/x11args
endef
else
define SENDAI_INSTALL_X11_ARGS
	echo -n "" > $(TARGET_DIR)/etc/x11args
endef
endif

# define SENDAI_INSTALL_DTOVERLAY
# 	echo "dtoverlay=${BR2_PACKAGE_SENDAI_CONFIG_DTOVERLAY}" >> $(BINARIES_DIR)/rpi-firmware/config.txt
# endef

# define SENDAI_INSTALL_DTPARAM
# 	echo "dtparam=${BR2_PACKAGE_SENDAI_CONFIG_DTPARAM}" >> $(BINARIES_DIR)/rpi-firmware/config.txt
# endef

# define SENDAI_INSTALL_BOOT_SPLASH
# 	sed -e "s/+1,@@/+1,$(ERPAIM_LOGO_LINES) @@/" package/sendai/kernel.patch_header > ${BUILD_DIR}/sendai_kernel.patch
# 	sed -e 's/^/+/' ${BR2_PACKAGE_SENDAI_BOOT_SPLASH} >> ${BUILD_DIR}/sendai_kernel.patch
# endef

define SENDAI_INSTALL_IMAGES_CMDS
	$(INSTALL) -D -m 0644 package/sendai/config.txt $(BINARIES_DIR)/rpi-firmware/config.txt
	$(INSTALL) -D -m 0644 package/sendai/cmdline.txt $(BINARIES_DIR)/rpi-firmware/cmdline.txt
	$(SENDAI_INSTALL_X11_ARGS)
	$(SENDAI_INSTALL_DTOVERLAY)
	$(SENDAI_INSTALL_DTPARAM)
	$(SENDAI_INSTALL_BOOT_SPLASH)
endef

$(eval $(generic-package))
