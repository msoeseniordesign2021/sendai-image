WM8960_DRIVERS_SITE_METHOD = git
WM8960_DRIVERS_VERSION = 0e427ada34b365e40b4b7822f74be96c6b57d589
WM8960_DRIVERS_SITE = git://github.com/waveshare/WM8960-Audio-HAT.git

ifeq ($(BR2_i386),y)
	WM8960_DRIVERS_TARGET_ARCH = x86
else ifeq ($(BR2_x86_64),y)
	WM8960_DRIVERS_TARGET_ARCH = x64
else ifeq ($(BR2_powerpc),y)
	WM8960_DRIVERS_TARGET_ARCH = ppc
else ifeq ($(BR2_arm)$(BR2_armeb),y)
	WM8960_DRIVERS_TARGET_ARCH = armv7-a
else ifeq ($(BR2_mips),y)
	WM8960_DRIVERS_TARGET_ARCH = mips
else ifeq ($(BR2_mipsel),y)
	WM8960_DRIVERS_TARGET_ARCH = mipsel
else
	WM8960_DRIVERS_TARGET_ARCH = $(BR2_ARCH)
endif


$(eval $(kernel-module))
$(eval $(generic-package))
