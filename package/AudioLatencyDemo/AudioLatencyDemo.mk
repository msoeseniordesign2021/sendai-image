AUDIOLATENCYDEMO_SITE_METHOD = git
AUDIOLATENCYDEMO_VERSION = 19437d8c7e8382ac089d3b0603cdef7ed0d5b58e
AUDIOLATENCYDEMO_SITE = git@gitlab.com:msoeseniordesign2021/audiolatencydemo.git
AUDIOLATENCYDEMO_DEPENDENCIES = alsa-lib freetype libcurl xlib_libX11 xlib_libXext xlib_libXcursor xlib_libXinerama xlib_libXrandr

ifeq ($(BR2_i386),y)
	AUDIOLATENCYDEMO_TARGET_ARCH = x86
else ifeq ($(BR2_x86_64),y)
	AUDIOLATENCYDEMO_TARGET_ARCH = x64
else ifeq ($(BR2_powerpc),y)
	AUDIOLATENCYDEMO_TARGET_ARCH = ppc
else ifeq ($(BR2_arm)$(BR2_armeb),y)
	AUDIOLATENCYDEMO_TARGET_ARCH = armv7-a
else ifeq ($(BR2_mips),y)
	AUDIOLATENCYDEMO_TARGET_ARCH = mips
else ifeq ($(BR2_mipsel),y)
	AUDIOLATENCYDEMO_TARGET_ARCH = mipsel
else
	AUDIOLATENCYDEMO_TARGET_ARCH = $(BR2_ARCH)
endif

define AUDIOLATENCYDEMO_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) $(TARGET_CONFIGURE_OPTS) CONFIG=Release TARGET_ARCH=-march=$(AUDIOLATENCYDEMO_TARGET_ARCH) -C $(@D)/Builds/LinuxMakefile
endef

define AUDIOLATENCYDEMO_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/Builds/LinuxMakefile/build/AudioLatencyDemo $(TARGET_DIR)/usr/bin/AudioLatencyDemo
endef

$(eval $(generic-package))
