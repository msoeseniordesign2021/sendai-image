AUDIOSETTINGSDEMO_SITE_METHOD = git
AUDIOSETTINGSDEMO_VERSION = 89bd28a5385b6004ec6e7c7a547ec2227d07107c
AUDIOSETTINGSDEMO_SITE = git@gitlab.com:msoeseniordesign2021/audiosettingsdemo.git
AUDIOSETTINGSDEMO_DEPENDENCIES = alsa-lib freetype libcurl xlib_libX11 xlib_libXext xlib_libXcursor xlib_libXinerama xlib_libXrandr

ifeq ($(BR2_i386),y)
	AUDIOSETTINGSDEMO_TARGET_ARCH = x86
else ifeq ($(BR2_x86_64),y)
	AUDIOSETTINGSDEMO_TARGET_ARCH = x64
else ifeq ($(BR2_powerpc),y)
	AUDIOSETTINGSDEMO_TARGET_ARCH = ppc
else ifeq ($(BR2_arm)$(BR2_armeb),y)
	AUDIOSETTINGSDEMO_TARGET_ARCH = armv7-a
else ifeq ($(BR2_mips),y)
	AUDIOSETTINGSDEMO_TARGET_ARCH = mips
else ifeq ($(BR2_mipsel),y)
	AUDIOSETTINGSDEMO_TARGET_ARCH = mipsel
else
	AUDIOSETTINGSDEMO_TARGET_ARCH = $(BR2_ARCH)
endif

define AUDIOSETTINGSDEMO_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) $(TARGET_CONFIGURE_OPTS) CONFIG=Release TARGET_ARCH=-march=$(AUDIOSETTINGSDEMO_TARGET_ARCH) -C $(@D)/Builds/LinuxMakefile
endef

define AUDIOSETTINGSDEMO_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/Builds/LinuxMakefile/build/AudioSettingsDemo $(TARGET_DIR)/usr/bin/AudioSettingsDemo
endef

$(eval $(generic-package))
