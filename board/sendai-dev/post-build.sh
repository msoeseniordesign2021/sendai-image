#!/bin/sh

set -u
set -e

# Add a console on tty1
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty1::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/GENERIC_SERIAL/a\
tty1::respawn:/sbin/getty -L  tty1 0 vt100 # HDMI console' ${TARGET_DIR}/etc/inittab
fi

sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' ${TARGET_DIR}/etc/ssh/sshd_config
sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords yes/g' ${TARGET_DIR}/etc/ssh/sshd_config
echo "replaced sshd_config settings"


ln -sf /usr/bin/Xfbdev ${TARGET_DIR}/usr/bin/X
echo "linked X to Xfbdev"


if [ -f "${TARGET_DIR}/etc/init.d/S40xorg" ] ; then
    rm "${TARGET_DIR}/etc/init.d/S40xorg"
    echo "remove xorg init"
fi

echo "replacing wm8960 soundcard overlay"
cp ${BUILD_DIR}/WM8960-Drivers-0e427ada34b365e40b4b7822f74be96c6b57d589/wm8960-soundcard.dtbo ${BINARIES_DIR}/rpi-firmware/